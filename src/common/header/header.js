import React, { Component } from 'react';
import { HeaderWrapper, HeaderSpan } from '../style';

export default class HeaderUI extends Component {

    render() {
        return (
        <HeaderWrapper>
            <HeaderSpan>TodoList Demo</HeaderSpan>
        </HeaderWrapper>
        );
    }
}