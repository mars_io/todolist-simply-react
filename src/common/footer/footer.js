import React, { Component } from "react";
import { Layout } from 'antd';
const { Footer } = Layout;

export default class FooterUI extends Component {

    render() {
        return (
            <Footer style={{ textAlign: 'center' }}>Just TodoList Demo @2019 by Mars</Footer>
        );
    }
}