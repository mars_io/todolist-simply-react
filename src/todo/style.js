import styled from 'styled-components';

const TagDiv = styled.div`
    text-align: left;
    margin-top: 20px;
    margin-bottom: 20px;
`;

const LoadingDiv = styled.div`
    text-align: center;
    margin-top: 12px;
    height: 32px;
    line-height: 32px;
`;


export {TagDiv, LoadingDiv};

