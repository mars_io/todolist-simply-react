import React from 'react';
import { Row, Col, Input, Button, Tag  } from 'antd';
import { TagDiv } from '../style';



const TodoForm = (props) => {

    return (
        <Row gutter={16}>
            <Col className="gutter-row" span={8}>
                <div className="gutter-box"></div>
            </Col>
            <Col className="gutter-row" span={6}>
                <div className="gutter-box">
                    <Input size="large" placeholder="Type todo info" 
                        value={props.inputValue} 
                        onChange={props.handleInputChange}
                    
                    />

                    <TagDiv>
                        <Tag color="#f50">#work</Tag>
                        <Tag color="#2db7f5">#traval</Tag>
                        <Tag color="#87d068">#books</Tag>
                        <Tag color="#108ee9">#study</Tag>
                    </TagDiv>


                </div>
            </Col>
            <Col className="gutter-row" span={4}>
                <div className="gutter-box">
                    <Button type="primary" shape="round" 
                        icon="form" size="large"
                        onClick={props.handleBtnClick}>
                        Add Quest
                    </Button>
                </div>
            </Col>
            <Col className="gutter-row" span={6}>
                <div className="gutter-box"></div>
            </Col>
        </Row>

    );
}

export default TodoForm;
