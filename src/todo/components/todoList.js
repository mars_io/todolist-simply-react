import React from 'react';
import { Row, Col, List, Avatar, Skeleton } from 'antd';
//import { LoadingDiv } from '../style';

const TodoList = (props) => {

  const list = props.list.results;
  
  return (
   
    <Row gutter={16}>
      <Col className="gutter-row" span={6}>
        <div className="gutter-box"></div>
      </Col>
      <Col className="gutter-row" span={12}>
        <div className="gutter-box">


          <List
            className="demo-loadmore-list"
            loading={false}
            itemLayout="horizontal"
            loadMore={false}
            dataSource={list}
            renderItem={item => (
              <List.Item actions={[<a>edit</a>, <a>more</a>]}>
                <Skeleton avatar title={false} loading={false} active>
                  <List.Item.Meta
                    avatar={
                      <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                    }
                    title={<a href="https://ant.design">{item.title}</a>}
                    description={item.description}
                  />
                  <div>{item.create_at}</div>
                </Skeleton>
              </List.Item>
            )}
          />

        </div>
      </Col>

      <Col className="gutter-row" span={6}>
        <div className="gutter-box"></div>
      </Col>
    </Row>
  );
}

export default TodoList;



