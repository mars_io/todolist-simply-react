import React, { Component, Fragment } from 'react';
import { Layout } from 'antd';
import HeaderUI from './common/header/header';
import "antd/dist/antd.css";
import FooterUI from './common/footer/footer';
import Todo from './todo/containers';
import {GlobalStyled} from './common/style'


const { Content } = Layout;


export default class App extends Component {

    render() {
        return (
            <Fragment>
                <GlobalStyled />
                <Layout className="layout">
                <HeaderUI />
                    <Content style={{ padding: '50px 50px 0px 50px' }}>
                        <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                            <Todo />
                        </div>
                    </Content>
                <FooterUI />
                </Layout>
            </Fragment>
        );
    }
}